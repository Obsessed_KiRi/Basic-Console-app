﻿namespace MathCommands;

public class ConsoleColorClass : IDisposable
{
    public void Dispose()
    {
        Console.ResetColor();
    }
    public ConsoleColorClass(string bgColorName, string fgColorName)
    {
        Console.BackgroundColor = Enum.TryParse(bgColorName, true, out ConsoleColor bgColor) ? bgColor : ConsoleColor.Black;
        Console.ForegroundColor = Enum.TryParse(fgColorName, true, out ConsoleColor fgColor) ? fgColor : ConsoleColor.White;
    }
}