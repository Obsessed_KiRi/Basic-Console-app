﻿namespace MathCommands;
public interface IOutputFactory
{
    TextWriter Create(string outputType);
}