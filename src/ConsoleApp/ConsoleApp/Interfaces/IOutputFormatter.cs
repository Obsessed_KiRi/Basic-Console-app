﻿using System.Text;

namespace MathCommands;
public interface IOutputFormatter
{
    string Format(int[] data);
}
