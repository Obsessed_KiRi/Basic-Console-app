﻿namespace MathCommands;
public interface IFormatFactory
{
    IFormatFactory OutputFormatter(string formatType, int columnsNumber);
}
