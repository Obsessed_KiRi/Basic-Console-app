﻿namespace MathCommands;
public interface ICommand
{
    void Execute();
}