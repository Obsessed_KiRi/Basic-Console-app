﻿using McMaster.Extensions.CommandLineUtils;
namespace MathCommands;

class Program
{
    static void Main(string[] args)
    {
        var app = new CommandLineApplication();

        var output = app.Option("-o|--output <TYPE>", "Type of output: file or console", CommandOptionType.SingleValue);
        output.DefaultValue = Constants.ConsoleOutput;

        var path = app.Option("--file <path>", "File path", CommandOptionType.SingleValue);
        path.DefaultValue = Constants.FilePath;

        var format = app.Option("--format <type>", "Format of output: line, columns, matrix.", CommandOptionType.SingleValue);
        format.DefaultValue = Constants.Line;

        var columns = app.Option<int>("-c|--columns <value>", "Inputs the number of columns in the matrix", CommandOptionType.SingleOrNoValue);

        var bgcolor = app.Option("-b|--bgcolor <colorName>", "Color of background: Black, Blue, Green, Cyan, Red, Magenta, Yellow, White.", CommandOptionType.SingleValue);
        bgcolor.DefaultValue = Constants.BackgroundColor;

        var fgcolor = app.Option("-f|--fgcolor <colorName>", "Color of output: Black, Blue, Green, Cyan, Red, Magenta, Yellow, White.", CommandOptionType.SingleValue);
        fgcolor.DefaultValue = Constants.ForegroundColor;

        app.HelpOption("-h|--help");

        app.Command(Constants.Fibonacci, fibCommand =>
        {
            fibCommand.Description = "You entered the command 'fib'";
            var number = fibCommand
                .Argument<int>("number", "Number of numbers in sequence")
                .IsRequired();

            fibCommand.OnExecute(() =>
            {
                var commandName = Constants.Fibonacci;

                int n = number.ParsedValue;

                var outputType = output.Value();
                var filePath = path.Value();

                var bgColorName = bgcolor.Value();
                var fgColorName = fgcolor.Value();

                var outputFactory = new OutputFactory(path.Value());
                var outputFormatter = new OutputFormatFactory().OutputFormatter(format.Value(), columns.ParsedValue);
                var commandFactory = new CommandFactory(outputFactory, outputFormatter);

                using ConsoleColorClass consoleColorManager = new(bgColorName, fgColorName);
                ICommand command = commandFactory.CreateCommand(commandName, n, outputType);
                command.Execute();
            });
        });

        app.Command(Constants.Fermat, ferCommand =>
        {
            ferCommand.Description = "You entered the command 'fer'";
            var number = ferCommand
                .Argument<int>("number", "Number of numbers in sequence")
                .IsRequired();

            ferCommand.OnExecute(() =>
            {
                var commandName = Constants.Fermat;

                int n = number.ParsedValue;

                var outputType = output.Value();
                var filePath = path.Value();

                var bgColorName = bgcolor.Value();
                var fgColorName = fgcolor.Value();


                var outputFactory = new OutputFactory(path.Value());
                var outputFormatter = new OutputFormatFactory().OutputFormatter(format.Value(), columns.ParsedValue);
                var commandFactory = new CommandFactory(outputFactory, outputFormatter);

                using ConsoleColorClass consoleColorManager = new(bgColorName, fgColorName);
                ICommand command = commandFactory.CreateCommand(commandName, n, outputType);
                command.Execute();
            });
        });

        try
        {
            app.Execute(args);
        }
        catch (UnauthorizedAccessException exception)
        {
            Console.WriteLine($"Error no access to file. {exception.Message}");
        }
        catch (FileNotFoundException exception)
        {
            Console.WriteLine($"Error file not found. {exception.Message}");
        }
        catch (Exception exception)
        {
            Console.WriteLine($"Error occurred: {exception.Message}");
        }
    }
}