﻿namespace MathCommands;
public static class Constants
{
    public const string Fibonacci = "fib";
    public const string Fermat = "fer";

    public const string ConsoleOutput = "console";
    public const string FilePath = "output.txt";
    public const string FileOutput = "file";

    public const string Line = "line";
    public const string Column = "column";
    public const string Matrix = "matrix";

    public const string BackgroundColor = "Black";
    public const string ForegroundColor = "White";
}