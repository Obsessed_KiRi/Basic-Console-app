﻿using System.Text;
namespace MathCommands;

public class MatrixOutputFormat : IOutputFormatter
{
    private int _columns;

    public MatrixOutputFormat(int columnsNumber)
    {
        _columns = columnsNumber;
    }

    public string Format(int[] data)
    {
        StringBuilder output = new StringBuilder();
        output.AppendLine("Matrix output: ");

        int PerRow = Math.Min(_columns, data.Length);
        for (int i = 0; i < data.Length; i++)
        {
            output.Append($"{data[i]}\t");

            if ((i + 1) % PerRow == 0)
            {
                output.AppendLine();
            }
        }
        return output.ToString();
    }
}