﻿namespace MathCommands;
using System.Text;
public class ColumnOutputFormat : IOutputFormatter
{
    public string Format(int[] data)
    {
        StringBuilder _result = new StringBuilder();
        _result.AppendLine("Column output: ");
        Array.ForEach(data, number => _result.AppendLine(number.ToString()));
        return _result.ToString();
    }
}