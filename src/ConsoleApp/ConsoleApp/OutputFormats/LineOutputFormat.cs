﻿using System.Text;

namespace MathCommands;

public class LineOutputFormat : IOutputFormatter
{
    public string Format(int[] data)
    {
        StringBuilder formattedData = new StringBuilder();
        formattedData.AppendLine("Line output: ");
        formattedData.Append(string.Join(", ", data));
        return formattedData.ToString();
    }
}
