﻿namespace MathCommands;

public class CommandFactory
{
    private IOutputFactory _outputFactory;
    private IOutputFormatter _outputFormatter;

    public CommandFactory(IOutputFactory outputFactory, IOutputFormatter outputFormatter)
    {
        _outputFactory = outputFactory;
        _outputFormatter = outputFormatter;
    }

    public virtual ICommand CreateCommand(string commandName, int n, string outputType)
    {
        TextWriter writer = _outputFactory.Create(outputType);

        return commandName switch
        {
            Constants.Fibonacci => new FibCommand(n, writer, _outputFormatter),
            Constants.Fermat => new FerCommand(n, writer, _outputFormatter),
            _ => throw new ArgumentException("Invalid command.", nameof(commandName))
        };
    }
}

