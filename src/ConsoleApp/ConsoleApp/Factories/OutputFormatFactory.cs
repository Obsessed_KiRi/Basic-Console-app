﻿namespace MathCommands;
public class OutputFormatFactory
{
    public IOutputFormatter OutputFormatter(string formatType, int columnsNumber)
    {
        return formatType switch
        {
            Constants.Line => new LineOutputFormat(),
            Constants.Column => new ColumnOutputFormat(),
            Constants.Matrix => new MatrixOutputFormat(columnsNumber),
            _ => throw new ArgumentException("Invalid format.", nameof(formatType))
        };
    }
}