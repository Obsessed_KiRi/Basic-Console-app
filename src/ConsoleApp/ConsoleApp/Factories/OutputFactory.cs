﻿namespace MathCommands;
public class OutputFactory : IOutputFactory
{
    private string _filePath;

    public OutputFactory(string filePath)
    {
        _filePath = filePath;
    }
    public TextWriter Create(string outputType) =>
      outputType switch
      {
          Constants.ConsoleOutput => Console.Out,
          Constants.FileOutput => new StreamWriter(_filePath),
          _ => throw new ArgumentException($"Invalid output type '{outputType}'.", nameof(outputType))
      };
}