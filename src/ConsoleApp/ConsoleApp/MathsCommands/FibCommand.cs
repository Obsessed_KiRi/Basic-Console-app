﻿using System.Text;

namespace MathCommands;
public class FibCommand : ICommand
{
    private int _number;
    private TextWriter _output;
    private IOutputFormatter _outputFormat;
    public FibCommand(int n, TextWriter output, IOutputFormatter outputFormat)
    {
        _number = n;
        _output = output;
        _outputFormat = outputFormat;
    }

    public void Execute()
    {
        int[] fibonacciResult = FibCalculate(_number);
        string output = _outputFormat.Format(fibonacciResult);
        _output.WriteLine(output);
    }

    private int[] FibCalculate(int n)
    {
        int[] fibSequence = new int[n];
        fibSequence[0] = 0;
        if (n > 1)
        {
            fibSequence[1] = 1;
            for (int i = 2; i < n; i++)
            {
                fibSequence[i] = fibSequence[i - 1] + fibSequence[i - 2];
            }
        }
        return fibSequence;
    }
}