﻿namespace MathCommands;
public class FerCommand : ICommand
{
    private int _number;
    private TextWriter _output;
    private IOutputFormatter _outputFormat;
    public FerCommand(int n, TextWriter output, IOutputFormatter outputFormat)
    {
        _number = n;
        _output = output;
        _outputFormat = outputFormat;
    }
    public void Execute()
    {
        int[] fermatResult = FerCalculate(_number);
        string output = _outputFormat.Format(fermatResult);
        _output.WriteLine(output);
    }
    private int[] FerCalculate(int n)
    {
        int[] fermatSequence = new int[n];
        fermatSequence[0] = 3;

        for (int i = 1; i < n; i++)
        {
            fermatSequence[i] = (int)(Math.Pow(2, Math.Pow(2, i)) + 1);
        }
        return fermatSequence;
    }
}
