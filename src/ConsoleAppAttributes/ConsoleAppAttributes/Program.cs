﻿using McMaster.Extensions.CommandLineUtils;

namespace MathCommands;

[Command(Description = "Generate sequence")]
[Subcommand(typeof(FibCommand))]
[Subcommand(typeof(FerCommand))]
[Subcommand(typeof(ReflectionCommand))]
class Program
{
    static void Main(string[] args)
    {
        try
        {
            var app = CommandLineApplication.Execute<Program>(args);
        }
        catch (UnauthorizedAccessException exception)
        {
            Console.WriteLine($"Error no access to file. {exception.Message}");
        }
        catch (FileNotFoundException exception)
        {
            Console.WriteLine($"Error file not found. {exception.Message}");
        }
        catch (Exception exception)
        {
            Console.WriteLine($"Error occurred: {exception.Message}");
        }
    }
}