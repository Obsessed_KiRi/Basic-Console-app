﻿using McMaster.Extensions.CommandLineUtils;

namespace MathCommands;

public abstract class MathCommandBase
{
    [Argument(0, Name = "number", Description = "Amount of numbers")]
    public int Number { get; set; }

    [Option("-o|--output <TYPE>", "Type of output: file or console", CommandOptionType.SingleValue)]
    public string Output { get; set; } = Constants.ConsoleOutput;

    [Option("--file <path>", "File path", CommandOptionType.SingleValue)]
    public string Path { get; set; } = Constants.FilePath;

    [Option("--format <type>", "Format of output: line, columns, matrix.", CommandOptionType.SingleValue)]
    public string Format { get; set; } = Constants.Line;

    [Option("-с|--columns <value> ", "Inputs the number of columns in the matrix", CommandOptionType.SingleOrNoValue)]
    public int Columns { get; set; }

    [Option("-b|--bgcolor <colorName>", "Color of background: Black, Blue, Green, Cyan, Red, Magenta, Yellow, White.", CommandOptionType.SingleValue)]
    public string BgColor { get; set; } = Constants.BackgroundColor;

    [Option("-f|--fgcolor <colorName>", "Color of output: Black, Blue, Green, Cyan, Red, Magenta, Yellow, White.", CommandOptionType.SingleValue)]
    public string FgColor { get; set; } = Constants.ForegroundColor;

    protected virtual void OnExecute()
    {
        using (TextWriter output = Create(Output))
        {
            SetConsoleColor();
            int[] sequence = Calculate(Number);
            string formattedData = OutputFormatter(Format, Columns).FormatData(sequence);
            output.WriteLine(formattedData);
            Reset();
        }
    }

    protected abstract int[] Calculate(int n);

    protected virtual TextWriter Create(string outputType)
    {
        return outputType switch
        {
            Constants.ConsoleOutput => Console.Out,
            Constants.FileOutput => new StreamWriter(Path),
            _ => throw new ArgumentException($"Invalid output type '{outputType}'.", nameof(outputType))
        };
    }

    protected IOutputFormatter OutputFormatter(string formatType, int columnsNumber)
    {
        return formatType switch
        {
            Constants.Line => new LineOutputFormat(),
            Constants.Column => new ColumnOutputFormat(),
            Constants.Matrix => new MatrixOutputFormat(columnsNumber),
            _ => throw new ArgumentException("Invalid format.", nameof(formatType))
        };
    }

    protected void SetConsoleColor()
    {
        ConsoleColorFunc(BgColor, FgColor);
    }

    private void ConsoleColorFunc(string bgColorName, string fgColorName)
    {
        Console.BackgroundColor = Enum.TryParse(bgColorName, true, out ConsoleColor bgColor) ? bgColor : ConsoleColor.Black;
        Console.ForegroundColor = Enum.TryParse(fgColorName, true, out ConsoleColor fgColor) ? fgColor : ConsoleColor.White;
    }

    public void Reset()
    {
        Console.ResetColor();
    }
}