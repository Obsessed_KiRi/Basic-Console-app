﻿using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Text;

namespace MathCommands;

[Command(Name = "ref", Description = "Showing some data")]

public class ReflectionCommand
{
    [Required]
    [Argument(0, Description = "Enter the name of the class you want to get information about")]

    public string ClassName { get; }
    public void OnExecute()
    {
        Type type;
        if (!string.IsNullOrEmpty(ClassName))
        {
            type = GetTypeByName(ClassName);
        }
        else
        {
            type = typeof(MathCommandBase);
        }

        TableInfo(type);
    }

    protected void TableInfo(Type type)
    {
        var table = new Table();
        table.AddColumn(new TableColumn("Information").Header("[blue]Data output[/]").Centered());

        table.AddRow(new Markup($"[yellow]Assembly information[/]"));
        table.AddRow(AssemblyInfo());

        table.AddEmptyRow();

        table.AddRow(new Markup($"[yellow]Type information[/]"));
        table.AddRow(TypeInfo(type));

        table.AddEmptyRow();

        table.AddRow(new Markup($"[yellow]Attribute information[/]"));
        table.AddRow(AttributeInfo(type));

        table.AddEmptyRow();

        table.AddRow(new Markup($"[yellow]Field information[/]"));
        table.AddRow(FieldInfo(type));

        table.AddEmptyRow();

        table.AddRow(new Markup($"[yellow]Property information[/]"));
        table.AddRow(PropertyInfo(type));

        table.AddEmptyRow();

        table.AddRow(new Markup($"[yellow]Methods information[/]"));
        table.AddRow(MethodInfo(type));

        AnsiConsole.Render(table);
    }


    protected string AssemblyInfo()
    {
        Assembly assembly = Assembly.GetExecutingAssembly();

        StringBuilder sb = new StringBuilder();
        sb.Append($"Assembly name: {assembly.GetName().Name}");
        sb.AppendLine($"Full name: {assembly.FullName}");
        sb.AppendLine($"Version: {assembly.GetName().Version}");
        sb.AppendLine($"Location: {assembly.Location}");
        return sb.ToString();
    }

    protected string TypeInfo(Type type)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine($"{type.FullName}");
        sb.AppendLine($"Namespace: {type.Namespace}");
        sb.AppendLine($"ShortName: {type.Name}");
        return sb.ToString();
    }

    protected string AttributeInfo(Type type)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var attribute in type.GetCustomAttributes())
        {
            var properties = attribute.GetType().GetProperties();
            sb.Append($"Attribute: {attribute.GetType().Name}");
            foreach (var property in properties)
            {
                var value = property.GetValue(attribute);
                sb.Append($"{property.Name}: {value}");
            }
        }
        return sb.ToString();
    }

    protected string FieldInfo(Type type)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var field in type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
        {
            sb.Append($"{field.Name} : {field.FieldType}");
        }
        return sb.ToString();
    }

    protected string PropertyInfo(Type type)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var property in type.GetProperties())
        {
            sb.Append($"{property.Name}");
        }
        return sb.ToString();
    }

    protected string MethodInfo(Type type)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var method in type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
        {
            sb.AppendLine("Method:");
            sb.Append($"{method.Name} : {method.ReturnType}");

            ParameterInfo[] parameters = method.GetParameters();
            if (parameters.Length > 0)
            {
                sb.AppendLine("Parameters:");
                foreach (ParameterInfo p in parameters)
                {
                    sb.Append($"Parameter name : {p.Name} \nParameter type : {p.ParameterType}");
                }
            }
            else
            {
                sb.AppendLine("No parameters");
            }
        }
        return sb.ToString();
    }

    protected virtual Type GetTypeByName(string className)
    {
        Type[] types = Assembly.GetExecutingAssembly().GetTypes();

        foreach (Type t in types)
        {
            if (t.Name == className || t.FullName == className)
            {
                return t;
            }
        }
        return null;
    }
}
