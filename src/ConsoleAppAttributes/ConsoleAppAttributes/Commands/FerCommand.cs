﻿using McMaster.Extensions.CommandLineUtils;

namespace MathCommands;

[Command(Name = "fer", Description = "Generate Fermat sequence")]
public class FerCommand : MathCommandBase
{
    protected override void OnExecute()
    {
        base.OnExecute();
    }
    protected override int[] Calculate(int n)
    {
        int[] sequence = new int[n];
        sequence[0] = 3;

        for (int i = 1; i < n; i++)
        {
            sequence[i] = (int)(Math.Pow(2, Math.Pow(2, i)) + 1);
        }
        return sequence;
    }
}
