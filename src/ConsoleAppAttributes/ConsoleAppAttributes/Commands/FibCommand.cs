﻿using McMaster.Extensions.CommandLineUtils;

namespace MathCommands;

[Command(Name = "fib", Description = "Generate Fibonacci sequence")]
public class FibCommand : MathCommandBase
{
    protected override void OnExecute()
    {
        base.OnExecute();
    }
    protected override int[] Calculate(int n)
    {
        int[] sequence = new int[n];
        sequence[0] = 0;
        if (n > 1)
        {
            sequence[1] = 1;
            for (int i = 2; i < n; i++)
            {
                sequence[i] = sequence[i - 1] + sequence[i - 2];
            }
        }
        return sequence;
    }
}