﻿namespace MathCommands;
using System.Text;

public class ColumnOutputFormat : IOutputFormatter
{
    public string FormatData(int[] data)
    {
        StringBuilder output = new StringBuilder();
        output.AppendLine("Column output: ");
        Array.ForEach(data, number => output.AppendLine(number.ToString()));
        return output.ToString();
    }
}