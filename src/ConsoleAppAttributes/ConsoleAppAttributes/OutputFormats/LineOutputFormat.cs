﻿namespace MathCommands;
using System.Text;

public class LineOutputFormat : IOutputFormatter
{
    public string FormatData(int[] data)
    {
        StringBuilder output = new StringBuilder();
        output.AppendLine("Line output: ");
        output.Append(string.Join(", ", data));
        return output.ToString();
    }
}
