﻿namespace MathCommands;
public interface IOutputFormatter
{
    string FormatData(int[] data);
}